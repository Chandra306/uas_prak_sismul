# No 1

Demo Video Processing
![videoProcessing](https://gitlab.com/Chandra306/uas_prak_sismul/-/blob/main/Video%20Processing/VideoProcessing__real_.gif)

# No 2

Youtube videoProcessing :

# No 3

Kode menggunakan library NumPy dan OpenCV dalam bahasa pemrograman Python. Kode ini berfungsi untuk merekam video menggunakan webcam dan menyimpannya dalam format file MP4.

Berikut adalah penjelasan singkat untuk setiap bagian kode tersebut:

1. Import library NumPy dan OpenCV:
   ```python
   import numpy as np
   import cv2
   ```

2. Menginisialisasi kamera dan pembuat video:
   ```python
   cap = cv2.VideoCapture(0)
   fourcc = cv2.VideoWriter_fourcc(*'XVID')
   out = cv2.VideoWriter('Test1.mp4', fourcc, 20.0, (640, 480))
   ```

   - `cap = cv2.VideoCapture(0)` menginisialisasi objek video capture untuk mengakses kamera dengan indeks 0 (biasanya kamera utama).
   - `fourcc = cv2.VideoWriter_fourcc(*'XVID')` menginisialisasi format video codec FourCC untuk kompresi video.
   - `out = cv2.VideoWriter('Test1.mp4', fourcc, 20.0, (640, 480))` membuat objek video writer untuk menyimpan video dengan nama file 'Test1.mp4', menggunakan codec yang telah ditentukan, frame rate 20 fps, dan ukuran frame 640x480.

3. Melakukan loop selama kamera terbuka:
   ```python
   while(cap.isOpened()):
       ret, frame = cap.read()
       if ret == True:
           # Menulis frame ke video
           out.write(frame)
           # Menampilkan frame di jendela output
           cv2.imshow('output', frame)
           # Menghentikan loop jika tombol 'q' ditekan
           if cv2.waitKey(1) & 0xFF == ord('q'):
               break
       else:
           break
   ```

   - `cap.read()` membaca frame dari kamera.
   - Jika frame berhasil dibaca (`ret == True`), maka frame ditulis ke video menggunakan `out.write(frame)`, ditampilkan di jendela output menggunakan `cv2.imshow('output', frame)`, dan loop berlanjut.
   - Jika frame tidak berhasil dibaca (`ret == False`), loop dihentikan.

4. Menutup objek kamera dan jendela output:
   ```python
   cap.release()
   cv2.destroyAllWindows()
   ```

   - `cap.release()` membebaskan sumber daya kamera yang digunakan.
   - `cv2.destroyAllWindows()` menutup semua jendela yang dibuat.

Dengan demikian, kode tersebut mengambil input video dari kamera, menulisnya ke file video, menampilkan output video secara real-time di jendela, dan berhenti saat tombol 'q' ditekan.
